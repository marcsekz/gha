//
// Created on 2018.10.03..
//

#include <stdint.h>

#ifndef SHA_2_SHA_256_H
#define SHA_2_SHA_256_H
#endif //SHA_2_SHA_256_H

#ifndef SET_WORD_VAL
#define SET_WORD_VAL
typedef uint32_t WORD;
typedef uint64_t WORD64;
#endif


unsigned char *sha256(char * msg, WORD64 size); // sha256 for text input
unsigned char *sha256file(char *path, int verbose);  // sha256 for files, reads one block at a time