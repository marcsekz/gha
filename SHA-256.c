#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "SHA-256.h"


/*
 * The SHA256 algorithm requires precise var lengths,
 * all additions must be performed as mod32,
 * size in bits must be stored on 64bits
 */

#ifndef SET_WORD_VAL
#define SET_WORD_VAL
typedef uint32_t WORD;
typedef uint64_t WORD64;
#endif

// Constants, Vars

WORD64 original_bits, bytes; // original length in bits. bytes stores the current length

WORD w[64];     // w: temporary array for a block.
// K: constants provided in SHA2 specification
WORD K[64] = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
              0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
              0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
              0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
              0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
              0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
              0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
              0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
// H: initial values for the 8 parts of the hash according to SHA2 spec
WORD H[8] = {0x6a09e667,
             0xbb67ae85,
             0x3c6ef372,
             0xa54ff53a,
             0x510e527f,
             0x9b05688c,
             0x1f83d9ab,
             0x5be0cd19};
// TH: temporary variable for the 8 parts of the hash
WORD TH[8];
// i,j: loop variables; T1,T2: hash calculation temporary values
WORD i,j,T1,T2;

WORD rotRight(WORD x,unsigned char n){  // right rotate by n bits
    return ((x >> n) | (x << (32-n)));
}

////////////////////
// SHA2 functions //
////////////////////

WORD CH(WORD x, WORD y, WORD z){
    return ((x&y)^((~x)&z));
}

WORD MAJ(WORD x, WORD y, WORD z){
    return ((x&y)^(x&z)^(y&z));
}

WORD SUM0(WORD x){
    return (rotRight(x,2)^rotRight(x,13)^rotRight(x,22));
}

WORD SUM1(WORD x){
    return (rotRight(x,6)^rotRight(x,11)^rotRight(x,25));
}

WORD SIG0(WORD x){
    return (rotRight(x,7)^rotRight(x,18)^(x>>3));
}

WORD SIG1(WORD x){
    return (rotRight(x,17)^rotRight(x,19)^(x>>10));
}

//////////////////////
// SHA256 algorithm //
//////////////////////

void sha256transform(char *msg){
    for (i = 0, j = 0; i < 16; i++, j += 4) {    // msg[first 64 bytes] -> w[first 16 WORDs]; big endian
        w[i] = (WORD) (((msg[j] << 24) & 0xFF000000) | ((msg[j+1] << 16) & 0xFF0000) |
                       ((msg[j+2] << 8) & 0xFF00) | (msg[j+3] & 0xFF));
    }

    for ( i = 16; i < 64; i++) {        // compute the remaining parts of w[], SHA" spec
        w[i] = SIG1(w[i-2]) + w[i-7] + SIG0(w[i-15]) + w[i-16];
    }

    for ( i = 0; i < 8; i++) {  // copy H[] to TH[]
        TH[i] = H[i];
    }

    // some actual SHA:

    for ( i = 0; i < 64; i++){
        T1 = TH[7] + SUM1(TH[4]) + CH(TH[4],TH[5],TH[6]) + K[i] + w[i];
        T2 = SUM0(TH[0]) + MAJ(TH[0],TH[1],TH[2]);
        TH[7] = TH[6];
        TH[6] = TH[5];
        TH[5] = TH[4];
        TH[4] = TH[3] + T1;
        TH[3] = TH[2];
        TH[2] = TH[1];
        TH[1] = TH[0];
        TH[0] = T1 + T2;
    }

    //end of fun part :(

    for ( i = 0; i < 8; i++) {  // add temporary hash values to H[] (previous state of the hash values)
        H[i] += TH[i];
    }

    for (i = 64; i < bytes; i++)    // copy every element of msg to 64 places
        msg[i - 64] = msg[i];

    bytes -= 64;                // msg length -= 64
    /*if (!bytes) {
        free(msg);
        msg = NULL;
    }
    else
        msg = realloc(msg, bytes);*/
}

unsigned char * sha256(char *msg, WORD64 size) {

    bytes = size;
    original_bits = size*8;

    /*
     * We process the input in 64B (512 bit) chunks, starting at the beginning.
     * After a chunk is processed, clip the first 64B of the message.
     * If the remaining msg is shorter than or equal to 64B, pad the message according to SHA2 specs.
     */

    while (bytes) {
        if (bytes > 64) {      // if remaining msg is longer, than 64B, process the first 64B
            sha256transform(msg);
            msg = (char*) realloc(msg, bytes);
        }
        else {
            if (bytes < 56) {
                msg = realloc(msg, 64);
                msg[bytes++] = (char) 0b10000000;        // append 1000 0000
                for (i = (WORD) bytes; i < 56; i++)      // fill with 0
                    msg[i] = 0;
                bytes = 64;
                for (i = 63, j = 0; i > 55; i--, j++) {  // append original size in bits
                    msg[i] = (char) ((original_bits >> (j * 8)) & 0xFF);
                }
                sha256transform(msg);
                msg = (char*) realloc(msg, bytes);
            }
            else {
                msg = realloc(msg, 128);
                msg[bytes++] = (char) 0x80;      // append 1000 0000
                for (i = (WORD) bytes; i < 120; i++)    // fill with 0
                    msg[i] = 0;
                for (i = 127, j = 0; i > 119; i--, j++) {  // append original size in bits
                    msg[i] = (char) ((original_bits >> (j * 8)) & 0xFF);
                }
                bytes = 128;
                sha256transform(msg);
                msg = (char*) realloc(msg, bytes);
                sha256transform(msg);
                msg = (char*) realloc(msg, bytes);
            }
        }
    }

    free(msg);
    unsigned char *result = (unsigned char *) malloc(32);

    for (i = 0, j = 0; i < 8; i++)
    {
        result[j++] = (unsigned char) (H[i] >> 24);
        result[j++] = (unsigned char) (H[i] >> 16);
        result[j++] = (unsigned char) (H[i] >> 8);
        result[j++] = (unsigned char) H[i];
    }

    return result;
}

unsigned char *sha256file(char *path, int verbose){
    WORD64 index = 0;
    clock_t start, stop;
    start = clock();
    FILE *file = NULL;
    bytes = 0;     // current block size in bytes, normally goes up to 64, or once 128
    WORD64 size = 0;      // sum size in bytes
    char *msg = (char *) malloc(64);
    char temp[64];
    WORD64 templen;
    int fileEnd = 0;

    // open file
    if (verbose)
        printf("Opening file %s\n",path);
    file = fopen(path,"rb");
    if (!file){
        printf("Failed to open file\n");
        return NULL;
    }

    if (verbose)
        printf("Start hashing\nProgress:\n");

    templen = fread(temp,1,64,file);

    // read one block, <= 64 bytes
    do {
        for (i = 0; i < 64; i++)
            msg[i] = temp[i];
        bytes = templen;
        size += bytes;

        if ( bytes < 64 )   // if we reached the EOF, and the last block is <64B
            fileEnd = 1;
        else
            templen = fread(temp,1,64,file);    // read one more block, this will be the next

        if (!templen)  // if the temp block is 0 length, then we've reached the end, but it was at a block border
            fileEnd = 1;

        if ( (bytes == 64) && !fileEnd ) {
            sha256transform(msg);
        }
        if (verbose) {
            index++;
            if (index > 500000) {
                index = 0;
                printf("%lu kB\t%lu MB\n", size / 1000, size / 1000000);
            }
        }
    } while (!fileEnd);

    if (bytes <= 64) {
        if (bytes < 56) {
            msg[bytes++] = (char) 0b10000000;        // append 1000 0000

            while (bytes < 56)      // fill with 0
                msg[bytes++] = 0;

            original_bits = size * 8;

            for (i = 63, j = 0; i > 55; i--, j++) {  // append original size in bits
                msg[i] = (char) ((original_bits >> (j * 8)) & 0xFF);
            }
            sha256transform(msg);
        } else {
            msg = (char *) realloc(msg, 128);
            msg[bytes++] = (char) 0b10000000;      // append 1000 0000
            while (bytes < 120)      // fill with 0
                msg[bytes++] = 0;

            original_bits = size * 8;

            for (i = 127, j = 0; i > 119; i--, j++) {  // append original size in bits
                msg[i] = (char) ((original_bits >> (j * 8)) & 0xFF);
            }
            bytes = 128;
            sha256transform(msg);
            sha256transform(msg);
        }
    }
    else{
        sha256transform(msg);
    }

    if ( msg != NULL)
        free(msg);
    unsigned char *result = (unsigned char *) malloc(32);

    for (i = 0, j = 0; i < 8; i++)
    {
        result[j++] = (unsigned char) (H[i] >> 24);
        result[j++] = (unsigned char) (H[i] >> 16);
        result[j++] = (unsigned char) (H[i] >> 8);
        result[j++] = (unsigned char) H[i];
    }
    if (verbose) {
        stop = clock();
        double time =((double)(stop - start))/CLOCKS_PER_SEC;
        printf("\nProcessed %lu bytes (%lu MB) in %.2f seconds\n", size,size/1000000 , time);
        printf("%.2f MB/s\n\n", (size/time)/1000000 );
    }
    return result;

}